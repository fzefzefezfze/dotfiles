#!/bin/sh

ln -sfn $PWD/x11/xinitrc $HOME/.xinitrc
ln -sfn $PWD/x11/xresources $HOME/.Xresources
sudo ln -sfn $PWD/x11/xorg/* /etc/X11/xorg.conf.d/
ln -sfn $PWD/autorandr $HOME/.config/
ln -sfn $PWD/systemd $HOME/.config/
ln -sfn $PWD/i3/ $HOME/.config/
ln -sfn $PWD/polybar/ $HOME/.config/
ln -sfn $PWD/termite/ $HOME/.config/
ln -sfn $PWD/zsh/zshrc $HOME/.zshrc
ln -sfn $PWD/rofi/applications/* $HOME/.local/share/applications/
ln -sfn $PWD/code/* $HOME/.config/Code/User/
ln -sfn $PWD/git/gitconfig $HOME/.gitconfig
#ln -s $PWD/nvim $HOME/.config
#ln -s $PWD/tmux/tmux.conf $HOME/.tmux.conf
#ln -s $PWD/zsh/zshrc $HOME/.zshrc