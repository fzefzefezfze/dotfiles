#!/usr/bin/env bash

dir="$HOME/dotfiles/rofi/styles"
theme="launcher"

rofi_command="rofi -theme $dir/$theme"

# Options
shutdown=" Shutdown"
reboot=" Reboot"
lock=" Lock"
options="$shutdown\n$reboot\n$lock"

chosen="$(echo -e "$options" | $rofi_command -dmenu -selected-row 2)"

case $chosen in
    $shutdown)
		systemctl poweroff
        ;;
    $reboot)
		systemctl reboot
        ;;
    $lock)
		betterlockscreen -l
        ;;
esac
